var express = require('express');
var fs = require('fs');
var sleep = require('sleep');

var app = express();
var port = 3333;

app.listen(port, () => {console.log('RESTful API server started on: ' + port);});

app.get('/help', (req, res) => {
  var name = Object.keys(req.query)[0];
  console.log(name);
  var filename = './data/' + name + '.json';

  if (fs.existsSync(filename)) {
    fs.readFile(filename, 'utf8', (err, data) => {
        if (err) {
          console.log(err);
          res.status(500);
          res.end(err);
        } else {
          console.log(data);
          sleep.sleep(5);
          res.status(200);
          res.setHeader('Content-Type', 'application/json');
          res.end(data);
        }
    });
  } else {
    res.status(500);
    res.end('unable to find file');
  }
});
